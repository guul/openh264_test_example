CC=g++ -g

encoder=encoder
decoder=decoder
enobjs=encode.o
deobjs=decode.o yuvchang.o
target=$(encoder) $(decoder)
objs=$(enobjs) $(deobjs)

libs= -lopenh264
lib_path=/usr/local/lib
use_opencv=`pkg-config --libs-only-l opencv`

target:$(target)

encoder:$(enobjs)
	$(CC) -o $(encoder) $(enobjs) -Wl,-rpath,$(lib_path) -L$(lib_path) $(libs)
decoder:$(deobjs)
	$(CC) -o $(decoder) $(deobjs) -Wl,-rpath,$(lib_path) -L$(lib_path) $(libs) $(use_opencv)

clean:
	rm $(target) $(objs) -f
